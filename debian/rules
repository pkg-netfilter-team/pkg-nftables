#!/usr/bin/make -f

ifeq (,$(filter terse,$(DEB_BUILD_OPTIONS)))
export DH_VERBOSE=1
endif
export PYBUILD_NAME = nftables
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

configure_opts := --with-xtables                     \
		  --with-json                        \
		  --with-python-bin=/usr/bin/python3 \
		  --with-cli=editline
pybuild_opts   := --buildsystem=pybuild -- --dir $(CURDIR)/py
pybuild_pkg    := python3-$(PYBUILD_NAME)

ifeq (,$(filter terse,$(DEB_BUILD_OPTIONS)))
configure_opts += --disable-silent-rules
pybuild_opts   += --verbose
else
configure_opts += --enable-silent-rules
pybuild_opts   += --quiet
endif

%:
	dh $@ --with python3

override_dh_auto_clean:
	dh_auto_clean -N$(pybuild_pkg)
	dh_auto_clean -p$(pybuild_pkg) $(pybuild_opts)

override_dh_auto_configure:
	dh_auto_configure -N$(pybuild_pkg) -- $(configure_opts)
	dh_auto_configure -p$(pybuild_pkg) $(pybuild_opts)

override_dh_auto_build:
	dh_auto_build -N$(pybuild_pkg)
	dh_auto_build -p$(pybuild_pkg) $(pybuild_opts)

override_dh_auto_install:
	dh_auto_install -N$(pybuild_pkg)
	dh_auto_install -p$(pybuild_pkg) $(pybuild_opts)

execute_after_dh_fixperms:
	chmod a+x debian/nftables/etc/nftables.conf

override_dh_installsystemd:
	dh_installsystemd --no-enable --no-start --restart-after-upgrade

override_dh_installexamples:
	dh_installexamples -XMakefile
